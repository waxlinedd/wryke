from __future__ import print_function
from wryke.wrike import Wrike
import sys
import tempfile
import unittest


class TestWrike(unittest.TestCase):
    def __init__(self, testname, user, folder_v2_id):
        super(TestWrike, self).__init__(testname)
        self.user = user
        self.folder_v2id = folder_v2id

    def setUp(self):
        self.wrk = Wrike()
        self.folder_v3id = self.wrk.find_v3id(self.folder_v2id, 'ApiV2Folder')

    def test_create_and_delete_task(self):
        task = self.wrk.create_task(folder_id=self.folder_v3id,
                                    title="test_create_task")
        task_v3id = task['id']
        self.assertEqual(len(self.wrk.tasks[task_v3id]()), 1)

        delete_response = self.wrk.tasks[task_v3id].delete()

        # Assert task is in the recycle bin
        self.assertEqual(self.wrk.tasks[task_v3id]()[0]['scope'], 'RbTask')


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('''Usage: test.py <email> <permalink (ApiV2) id of test folder in Wrike>
eg: test.py myemail@nowhere.net 309823175''')
        exit(0)
    user = sys.argv[1]
    folder_v2id = sys.argv[2]

    test_loader = unittest.TestLoader()
    test_names = test_loader.getTestCaseNames(TestWrike)

    suite = unittest.TestSuite()
    for test_name in test_names:
        suite.addTest(TestWrike(test_name, user, folder_v2id))

    result = unittest.TextTestRunner().run(suite)
    sys.exit(not result.wasSuccessful())
