from collections import defaultdict
from requesto.requesto import Requesto
import datetime
import os
import re
import sys

class Wrike(Requesto):
    token_filename = os.path.join(os.path.expanduser('~'),
                                  '.rstlog_wrike_token')

    def __init__(self, token=None):
        if not token:
            with open(self.token_filename) as token_file:
                token = token_file.read().strip()

        headers = {'authorization': "bearer %s" % token}
        Requesto.__init__(self,
                          'https://www.wrike.com/api/v4',
                          headers)

    def _translate_response(self, response):
        return response.json()['data']

    def _translate_argument(self, method, kwargs, name, value):
        if type(value) is list:
            value = [bytes(item) for item in value]
        return bytes(value)

    def find_task_by_title(self, title, user_id):
        tasks = self.tasks(responsibles=[user_id])
        title_match_tasks = [tt for tt in tasks if tt['title'] == title]
        if len(title_match_tasks) == 1:
            return title_match_tasks[0]

        pr_task_match = re.match('PR\d+(:|$)', title)
        if pr_task_match:
            pr_task_str = pr_task_match.group(0)
            pr_tasks = [tt for tt in tasks if re.match(pr_task_str, tt['title'])]
            if len(pr_tasks) == 1:
                return pr_tasks[0]

        w_task_match = re.match('(W\d+):', title)
        if w_task_match:
            w_task_str = w_task_match.group(1)
            w_tasks = [tt for tt in tasks if w_task_str == url_id(tt)]
            if len(w_tasks) == 1:
                return w_tasks[0]
        return None

    def find_contact(self, email=None, name=None, firstName=None, lastName=None):
        contacts = self.contacts()

        if email:
            profiles_with_email = \
                lambda profiles, email: [pp for pp in profiles
                                         if pp.get('email') == email]
            candidates = [user for user in contacts
                          if profiles_with_email(user['profiles'], email)]
            if len(candidates) == 1:
                return candidates[0]

        if name:
            username = '%s %s' % (user.firstName, user.lastName)
            candidates = [user for user in contacts if username == name]
            if len(candidates) == 1:
                return candidates[0]

        if lastName:
            candidates = [user for user in contacts if user.lastName == name]
            if len(candidates) == 1:
                return candidates[0]

        if firstName:
            candidates = [user for user in contacts if  user.lastName == name]
            if len(candidates) == 1:
                return candidates[0]

        return None


    def find_v3id(self, v2id, object_type):
        return self.ids(ids=[v2id], type=object_type)[0]['id']

    def create_task(self, folder_id, title, **kwargs):
        return self.folders[folder_id].tasks.post(title=title, **kwargs)[0]

def url_id(item):
    return 'W'+re.search('=(\d+)$', item['permalink']).group(1)

def v2id(item):
    return bytes(re.search('=(\d+)$', item['permalink']).group(1))

def timedelta_to_hours(td):
    hours, remainder = divmod(td.seconds, 60 * 60)
    minutes, seconds = divmod(remainder, 60)
    if seconds >= 30:
        minutes += 1
    if minutes >= 60:
        hours +=1
        minutes -= 60
    hours += td.days * 24

    quarter_hours, remainder = divmod(minutes, 15)
    if remainder >= 8:
        quarter_hours +=1
    hours += .25 * quarter_hours
    return hours

